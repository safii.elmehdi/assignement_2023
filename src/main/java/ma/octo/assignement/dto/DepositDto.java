package ma.octo.assignement.dto;


import lombok.*;

import java.math.BigDecimal;


@Getter
@Setter
public class DepositDto {

    private String nom_prenom_emetteur;
    private String rib;
    private String motif;
    private BigDecimal montant;

}
