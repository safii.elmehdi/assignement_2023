package ma.octo.assignement.exceptions;

import java.io.Serial;

public class CompteNonExistantException extends Exception {

  @Serial
  private static final long serialVersionUID = 1L;

  public CompteNonExistantException() {
  }

  public CompteNonExistantException(String message) {
    super(message);
  }
}
