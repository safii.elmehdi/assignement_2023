package ma.octo.assignement.exceptions;

import java.io.Serial;

public class DepositException extends Exception {

  @Serial
  private static final long serialVersionUID = 1L;

  public DepositException() {
  }

  public DepositException(String message) {
    super(message);
  }
}
