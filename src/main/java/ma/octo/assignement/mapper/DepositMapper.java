package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;

public class DepositMapper {

    private static DepositDto depositDto;

    public static DepositDto map(MoneyDeposit deposit) {
        depositDto = new DepositDto();
        depositDto.setNom_prenom_emetteur(deposit.getNom_prenom_emetteur());
        depositDto.setMontant(deposit.getMontant());
        depositDto.setMotif(deposit.getMotifDeposit());

        return depositDto;

    }

}
