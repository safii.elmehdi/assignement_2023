package ma.octo.assignement.service;

import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.mapper.DepositMapper;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.service.interfaces.IDepositService;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.DepositException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.DepositRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class DepositService implements IDepositService {
    public static final int MONTANT_MAXIMAL = 10000;

    Logger LOGGER = LoggerFactory.getLogger(DepositService.class);

    @Autowired
    private DepositRepository depositRepository;

    @Autowired
    private CompteRepository compteRepository;

    @Autowired
    private AuditService auditService;

    @Override
    public List<DepositDto> findAll() {
        LOGGER.info("Lister des deposits");
        var deposits = depositRepository.findAll();
        if (deposits.size() == 0) {
            return null;
        }
        List<DepositDto> depositDtoList = new ArrayList<>();
        deposits.forEach(deposit -> depositDtoList.add(DepositMapper.map(deposit)));
        return depositDtoList;
    }

    @Override
    public DepositDto createDeposit(DepositDto depositDto) throws DepositException, CompteNonExistantException {
        Compte compteBeneficiaire = compteRepository.findByRib(depositDto.getRib());
        String msg;

        if(depositDto.getRib() == null){
            msg="Rib vide";
            System.out.println(msg);
            throw new DepositException(msg);
        }else if (compteBeneficiaire == null) {
            msg="Compte Non existant";
            System.out.println(msg);
            throw new CompteNonExistantException(msg);
        }else if (depositDto.getMontant() == null) {
            System.out.println("Montant vide");
            throw new DepositException("Montant vide");
        }else if (depositDto.getMontant().intValue() == 0) {
            System.out.println("Montant vide");
            throw new DepositException("Montant vide");
        } else if (depositDto.getMontant().intValue() > MONTANT_MAXIMAL) {
            System.out.println("Montant maximal de deposit dépassé");
            throw new DepositException("Montant maximal de deposit dépassé");
        }

        if( depositDto.getNom_prenom_emetteur() == null ){
            System.out.println("Le nom d'emetteur est vide");
            throw new DepositException(("Emetteur Nom et Prenom vide"));
        }
        if (depositDto.getMotif() == null) {
            System.out.println("Motif vide");
            throw new DepositException("Motif vide");
        }

        compteBeneficiaire.setSolde(new BigDecimal(compteBeneficiaire.getSolde().intValue() +  depositDto.getMontant().intValue()));
        compteRepository.save(compteBeneficiaire);

        MoneyDeposit deposit = new MoneyDeposit();
        deposit.setMontant(depositDto.getMontant());
        deposit.setCompteBeneficiaire(compteBeneficiaire);
        deposit.setMotifDeposit(depositDto.getMotif());
        deposit.setNom_prenom_emetteur(depositDto.getNom_prenom_emetteur());

        depositRepository.save(deposit);

        auditService.auditDeposit("Deposit vers le compt de RIB"+ depositDto.getRib()+" par  "+ depositDto.getNom_prenom_emetteur());

        return DepositMapper.map(deposit);
    }
}
