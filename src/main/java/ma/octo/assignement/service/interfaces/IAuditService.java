package ma.octo.assignement.service.interfaces;

public interface IAuditService {

    void auditTransfer(String message);

    void auditDeposit(String message);
}
