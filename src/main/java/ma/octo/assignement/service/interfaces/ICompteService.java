package ma.octo.assignement.service.interfaces;

import ma.octo.assignement.domain.Compte;

import java.util.List;

public interface ICompteService {
    List<Compte> loadAllCompte();
}
