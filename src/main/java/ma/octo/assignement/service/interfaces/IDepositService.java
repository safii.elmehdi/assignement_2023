package ma.octo.assignement.service.interfaces;

import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.DepositException;

import java.util.List;


public interface IDepositService {

    List<DepositDto> findAll();
    DepositDto createDeposit(DepositDto deposit)
            throws DepositException, CompteNonExistantException;
}
