package ma.octo.assignement.service.interfaces;

import ma.octo.assignement.domain.Utilisateur;

import java.util.List;

public interface IUtilisateurService {
    List<Utilisateur> loadAllUtilisateur();
}
