package ma.octo.assignement.web;

import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.DepositException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.interfaces.IDepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(path = "/deposit")
public class DepositController {

    @Autowired
    private IDepositService depositService;

    @GetMapping
    public List<DepositDto> findAll() {
        return depositService.findAll();
    }

    @PostMapping
    public ResponseEntity<DepositDto> createDeposit(@RequestBody DepositDto depositDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, DepositException {
        DepositDto depositDto1 = depositService.createDeposit(depositDto);

        return ResponseEntity.status(HttpStatus.CREATED).body(depositDto1);
    }
}
