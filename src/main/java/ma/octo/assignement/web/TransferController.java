package ma.octo.assignement.web;


import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.interfaces.ITransferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/transfers")
public class TransferController {

    Logger LOGGER = LoggerFactory.getLogger(TransferController.class);

    @Autowired
    private ITransferService transferService;

    @GetMapping
    public List<TransferDto> loadAll() {
        return transferService.loadAll();
    }

    @PostMapping
    public ResponseEntity<TransferDto> createTransaction(@RequestBody TransferDto transferDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
        TransferDto transferDto1 = transferService.createTransaction(transferDto);

        return ResponseEntity.status(HttpStatus.CREATED).body(transferDto1);
    }
}
